a, b, c = [int(x) for x in input().split(" ")]
if a == b + c:
    print(str(a) + "=" + str(b) + "+" + str(c))
elif a == b - c:
    print(str(a) + "=" + str(b) + "-" + str(c))
elif a == b * c:
    print(str(a) + "=" + str(b) + "*" + str(c))
elif a == b / c:
    print(str(a) + "=" + str(b) + "/" + str(c))
elif a + b == c:
    print(str(a) + "+" + str(b) + "=" + str(c))
elif a - b == c:
    print(str(a) + "-" + str(b) + "=" + str(c))
elif a * b == c:
    print(str(a) + "*" + str(b) + "=" + str(c))
elif a / b == c:
    print(str(a) + "/" + str(b) + "=" + str(c))