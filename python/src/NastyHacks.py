n = int(input())
R = []
rR = 0
for i in range(n):
    R.append(input())
    r, e, c = R[i].split(" ")
    rR = int(e) - int(r) - int(c)
    if rR > 0:
        print("advertise")
    elif rR == 0:
        print("does not matter")
    else:
        print("do not advertise")