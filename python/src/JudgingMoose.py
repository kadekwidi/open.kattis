inp = input().split(" ")
L = int(inp[0])
R = int(inp[1])
if L != 0 and R != 0:
    if L == R:
        print("Even", (L + R))
    elif L > R:
        print("Odd", (L * 2))
    else:
        print("Odd", (R * 2))
elif L == 0 and R != 0:
    print("Odd", (R * 2))
elif L != 0 and R == 0:
    print("Odd", (L * 2))
else:
    print("Not a Moose")