n, B = input().split(" ")
N = int(n)
dominant = [11, 4, 3, 20, 10, 14, 0, 0]
not_dominant = [11, 4, 3, 2, 10, 0, 0, 0]
num = ['A', 'K', 'Q', 'J', 'T', '9', '8', '7']
data = []
total = 0
for i in range(N * 4):
    data.append(input())
for j in range(N*4):
    if data[j][1] == B:
        for i in range(8):
            if data[j][0] == num[i]:
                total += dominant[num.index(data[j][0])]
    else:
        for i in range(8):
            if data[j][0] == num[i]:
                total += not_dominant[num.index(data[j][0])]
print(total)
