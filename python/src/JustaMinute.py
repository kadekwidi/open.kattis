nKereta = int(input())
terprediksi = 0
benar = 0
for i in range(nKereta):
    a, b = input().split(" ")
    terprediksi += int(a) * 60
    benar += int(b)
hasil = benar / terprediksi
if hasil > 1:
    print("%.9f" % (hasil))
else:
    print("measurement error")