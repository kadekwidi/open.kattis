abjad = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ_.'

while True:
    inp = input().split()
    rotate = inp[0]
    if rotate == "0":
        break
    text = inp[1]
    text = text[::-1]
    for letter in text:
        idx = abjad.find(letter) + int(rotate)
        if idx >= 27:
            idx -= 28
        print(abjad[idx], end='')
    print()