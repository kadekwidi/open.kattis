import math

num = input()
n, w, h = num.split(" ")
N = []
l = math.sqrt((int(w) ** 2) + (int(h) ** 2))
for i in range(int(n)):
    N.append(int(input()))
    if N[i] <= l:
        print("DA")
    else:
        print("NE")
