inp = input()
word = inp.split(" ")
dup = False
for i in range(len(word)):
    for j in range(len(word)):
        if i != j:
            if word[i] == word[j]:
                dup = True
                break
if dup:
    print("no")
else:
    print("yes")