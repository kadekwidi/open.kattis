import math

case = 1
while True:
    n = int(input())
    if n == 0:
        break
    else:
        teks = []
        ordr = []
        for i in range(n):
            teks.append(input())
        ordr = sorted(teks, key=len)
        l = 0
        rev = False
        print("SET", case)
        for i in range(n):
            print(ordr[l])
            if not rev:
                if l + 2 < len(ordr):
                    l += 2
                elif l == len(ordr) - 1:
                    l -= 1
                    rev = True
                elif l + 2 > len(ordr) - 1:
                    if len(ordr) % 2 == 0:
                        l += 1
                    else:
                        l += 2
                    rev = True
            else:
                l -= 2
        case += 1
