# input H M
# M - 45
#
# Example
# in : 10 10
# out: 9 25

clock = input()
h, m = clock.split(" ")
H = int(h)
M = int(m)
i = 45
while i > 0:
    i -= 1
    M-=1
    if H < 0:
        H = 23
    if M < 0:
       M = 59
       H -= 1
print(str(H) + " " + str(M))