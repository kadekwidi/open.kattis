hari_dalam_bulan = {
    1: 30,
    2: 28,
    3: 31,
    4: 30,
    5: 31,
    6: 30,
    7: 31,
    8: 31,
    9: 30,
    10: 31,
    11: 30,
    12: 31,
}
lompatan_tahun = []
for tahun in range(0, 999):
    if (tahun) % 4 == 0:
        lompatan_tahun.append(tahun)


def is_lompatan_tahun(tahun):
    return tahun in lompatan_tahun

inp = input()
b, h, t = inp.split("/")

hari = int(h)
bulan = int(b)
if int(t) < 2000:
    tahun = int(t)
else:
    tahun = int(t) - 2000

batas_hari = int(hari_dalam_bulan.get(bulan, bulan)) # hari dalam bulan

if is_lompatan_tahun(tahun): # cek tahun
    if bulan > 0 and bulan <= 12: # cek bulan
        if bulan == 2: # bulan februari
            if hari > 0 and hari <= batas_hari + 1: # cek hari
                print(str(tahun + 2000) + "-", end='')
                if bulan > 9:
                    print(str(bulan) + "-", end='')
                    if hari > 9:
                        print(hari)
                    else:
                        print("0"+str(hari))
                else:
                    print("0" + str(bulan) + "-", end='')
                    if hari > 9:
                        print(hari)
                    else:
                        print("0"+str(hari))
            else:
                print(inp, "is illegal")
        else: # selain bulan februari
            if hari > 0 and hari <= batas_hari: # cek hari
                print(str(tahun + 2000) + "-", end='')
                if bulan > 9:
                    print(str(bulan) + "-", end='')
                    if hari > 9:
                        print(hari)
                    else:
                        print("0"+str(hari))
                else:
                    print("0" + str(bulan) + "-", end='')
                    if hari > 9:
                        print(hari)
                    else:
                        print("0"+str(hari))
            else:
                print(inp, "is illegal")
    else:
        print(inp, "is illegal")
else:
    if bulan > 0 and bulan <= 12: # cek bulan
        if bulan == 2: # bulan februari
            if hari > 0 and hari <= batas_hari: # cek hari
                print(str(tahun + 2000) + "-", end='')
                if bulan > 9:
                    print(str(bulan) + "-", end='')
                    if hari > 9:
                        print(hari)
                    else:
                        print("0"+str(hari))
                else:
                    print("0" + str(bulan) + "-", end='')
                    if hari > 9:
                        print(hari)
                    else:
                        print("0"+str(hari))
            else:
                print(inp, "is illegal")
        else: # selain bulan februari
            if hari > 0 and hari <= batas_hari: # cek hari
                print(str(tahun + 2000) + "-", end='')
                if bulan > 9:
                    print(str(bulan) + "-", end='')
                    if hari > 9:
                        print(hari)
                    else:
                        print("0"+str(hari))
                else:
                    print("0" + str(bulan) + "-", end='')
                    if hari > 9:
                        print(hari)
                    else:
                        print("0"+str(hari))
            else:
                print(inp, "is illegal")
    else:
        print(inp, "is illegal")