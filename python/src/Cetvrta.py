### EXAMPLE ###
# A = 1, 2
# B = 1, 4
# C = 3, 2
# D = xD, yD

#      a  b                 yD = By 
# X = (A, B) = ((1, 2), (1, 4))

#      a  b              xD = Cx
# Y = (A, C) = ((1, 2), (3, 2))

# 

#   7
#   6
#   5
#   4 B---D
#   3 |   |
#Y  2 A---C
#   1
#   0 1 2 3 4 5 6 7
#     X
################################

x = []
y = []
for i in range(3):
    coor = input()
    x.append(coor.split(" ")[0])
    y.append(coor.split(" ")[1])
    # x = [1, 1, 3]
    # y = [2, 4, 2]
X = [] # X
Y = [] # Y
for i in range(3):
    for j in range(3):
        if i != j:
            if x[i] == x[j]: # x[0] == x[1] and x[1] == x[0]
                X.append(x[i])  # X = Ax = Bx
                # X will store 2 data [ax, bx]
    for j in range(3):
        if i != j:
            if y[j] == y[i]: # y[0] == y[2] and y[2] == y[0]
                Y.append(y[i])  # Y = Ay = By
                # Y will store 2 data -> [ay, by]
xD = 0 # xD
yD = 0 # yD
for i in range(3):
    if X[0] != x[i]: # X[0] != Cx
        xD = x[i] # based on example, xD = Cx
    if Y[0] != y[i]: # if Y[0] != all y coordinates (Ay, By, or Cy)
        yD = y[i] # based on example, yD = By
print(xD, yD)