import math

a, v = [float(x) for x in input().split(" ")]
print(math.ceil(a / math.sin(math.radians(v))))