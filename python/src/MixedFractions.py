inp = input()
a, b = inp.split(" ")
num = 0
if int(b) != 0:
    if int(a) % int(b) == 0:
        print(round(int(a) / int(b)))
    elif int(a) > int(b):
        num = int(a) % int(b)
        print(str(round(int(a) / int(b))) + " " + str(num) + " / " + b)
    else:
        print("0 " + a + " / " + b)