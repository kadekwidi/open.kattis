
n = int(input())
a = []
b = []
c = []
for i in range(n):
    inp = input()
    x = int(inp.split(" ")[0])
    y = int(inp.split(" ")[1])
    z = int(inp.split(" ")[2])
    a.append(x)
    b.append(y)
    c.append(z)
for i in range(n):
    print("Possible" if c[i] in [a[i] + b[i], a[i] - b[i], b[i] - a[i], a[i] * b[i], a[i] / b[i], b[i] / a[i]] else "Impossible")
