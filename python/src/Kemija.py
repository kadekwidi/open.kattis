inp = input()
vokal = ["a", "e", "i", "o", "u"]
kalimat = ""
i = 0
while i < len(inp):
    huruf = inp[i]
    kalimat += huruf
    if huruf in vokal:
        i += 3
    else:
        i += 1
print(kalimat)
