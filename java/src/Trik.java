import java.util.Scanner;

class Trik {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int pos = 1;
		for (char c : sc.nextLine().toCharArray()) {
			if (pos == 1) {
				if (c == 'A') {
					pos = 2;
				} else if (c == 'C') {
					pos = 3;
				}
			} else if (pos == 2) {
				if (c == 'A') {
					pos = 1;
				} else if (c == 'B') {
					pos = 3;
				}
			} else {
				if (c == 'B') {
					pos = 2;
				} else if (c == 'C') {
					pos = 1;
				}
			}
		}
		System.out.println(pos);
	}
}