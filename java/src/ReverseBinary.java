
import java.util.Scanner;

class ReverseBinary {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        long num;
        int getNum = in.nextInt();
        if (getNum % 512 == 0) {
            num = (long) getNum / 512;
        } else {
            num = (long) getNum;
        }
        String bin = Long.toBinaryString(num);
        String revBin = "";
        for (int i = bin.length(); i > 0; i--) {
            revBin += bin.charAt(i - 1) + "";
        }
        char[] cBin = revBin.substring(revBin.indexOf("1"), revBin.length()).toCharArray();
        int result = 0;
        for (int i = 1; i <= cBin.length; i++) {
            if (cBin[i-1] == '1') {
                result += Math.pow(2, cBin.length - i);
            }
        }

        System.out.println(result);
        // String setNum = Long.parseLong(revBin.substring(revBin.indexOf("1"),
        // revBin.length())) + "";
        // System.out.println(Long.parseLong(revBin.substring(revBin.indexOf("1"),
        // revBin.length()), 2));
    }
}