import java.util.Scanner;

class Modulo{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		int count = 0;
		int input;
		for(int i = 0; i < 10; i++){
			input = sc.nextInt();
			if(input % 42 != 0){
				count++;
			} else {
				count--;
			}
		}
		System.out.println(count);
	}
}