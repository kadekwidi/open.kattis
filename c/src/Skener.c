#include <stdio.h>

int main()
{
    int r, c, zr, zc, xr, xc;
    scanf("%d%d%d%d%d%d", &r, &c, &zr, &zc, &xr, &xc);
    char s[r][c];
    for (int i = 0; i < r; i++)
    {
        scanf("%s", s[i]);
    }
    printf("\n");
    for (int g = 0; g < xr; g++)
    {
        for (int h = 0; h < xc; h++)
        {
            for (int i = 1; i <= r; i++)
            {
                for (int j = 1; j <= zr; j++)
                {
                    for (int k = 1; k <= c; k++)
                    {
                        for (int l = 1; l <= zc; l++)
                        {
                            printf("%c", s[i - 1][k - 1]);
                        }
                    }
                    printf("\n");
                }
            }
        }
    }
    return 0;
}